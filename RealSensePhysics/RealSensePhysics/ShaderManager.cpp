#include "ShaderManager.h"

// Loads and compiles all Shaders
void ShaderManager::Init()
{
	LoadShaders();
	CompileShaders();
}


// Reads in shaders, must load in the same order as listed in ShaderType enum
void ShaderManager::LoadShaders()
{
	Shader shader;

	shader.Load("SimpleVertShader.txt", "SimpleFragShader.txt");
	shaders.push_back(shader);

	shader.Load("SphereVertShader.txt", "SphereFragShader.txt");
	shaders.push_back(shader);

	shader.Load("DepthVert.txt", "DepthFrag.txt");
	shaders.push_back(shader);

	shader.Load("NormalsVert.txt", "NormalsFrag.txt");
	shaders.push_back(shader);

	shader.Load("BlurVert.txt", "BlurFrag.txt");
	shaders.push_back(shader);

	shader.Load("ThicknessVert.txt", "ThicknessFrag.txt");
	shaders.push_back(shader);

	shader.Load("FinalVert.txt", "FinalFrag.txt");
	shaders.push_back(shader);

	shader.Load("TextureVertShader.txt", "TextureFragShader.txt");
	shaders.push_back(shader);

	shader.Load("SkyVertShader.txt", "SkyFragShader.txt");
	shaders.push_back(shader);

	shader.Load("ClothVertShader.txt", "ClothFragShader.txt");
	shaders.push_back(shader);

	CompileShaders();
}


// Compiles each shader, put stops in shader.cpp code to debug errors
void ShaderManager::CompileShaders()
{
	for (unsigned int i=0; i<shaders.size(); i++)
	{
		shaders[i].CompileShaders();
	}
}


// Takes an enum of the type of shader desired, activates it and passes back an ID
GLuint ShaderManager::UseShader(ShaderType shaderType)
{
	GLuint ID = shaders[(int)shaderType].ID;
	glUseProgram (ID);

	return ID;
}