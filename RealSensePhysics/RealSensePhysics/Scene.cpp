#include "Scene.h"

void Scene::Init(int screenWidth, int screenHeight)
{
	camera.Init(screenWidth, screenHeight);
	shaderManager.Init();
	environmentMap.Init();
	fluidRenderer.Init(screenWidth, screenHeight);
	physicsWorld.Init();
}

void Scene::Update()
{

	camera.Update();
	realSense.Update();
	physicsWorld.Update(realSense.numHandPoints, &realSense.handPoints[0]);

	Draw();
}

void Scene::Draw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

//	physicsWorld.Draw(camera, shaderManager);
	GLuint shaderProgramID = shaderManager.UseShader(ClothShader);

	glm::mat4 model_mat = glm::mat4(1.0f);
	GLint uniformLoc = glGetUniformLocation (shaderProgramID, "proj");
	glUniformMatrix4fv (uniformLoc, 1, GL_FALSE, &camera.projMat[0][0]);
	uniformLoc = glGetUniformLocation (shaderProgramID, "view");
	glUniformMatrix4fv (uniformLoc, 1, GL_FALSE, &camera.viewMat[0][0]);
	uniformLoc = glGetUniformLocation (shaderProgramID, "model");
	glUniformMatrix4fv (uniformLoc, 1, GL_FALSE, &model_mat[0][0]);
	physicsWorld.cloth.Draw(shaderProgramID, physicsWorld.particleSystem);

//	fluidRenderer.Render(physicsWorld, environmentMap, camera, shaderManager);
//	environmentMap.Draw(camera, shaderManager);
	realSense.DrawPoints(camera, shaderManager);

	glutSwapBuffers();
}