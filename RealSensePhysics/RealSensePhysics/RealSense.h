#ifndef REAL_SENSE_H
#define REAL_SENSE_H

#include "pxcsession.h"
#include "pxccapture.h"
#include "pxchandmodule.h"
#include "pxchanddata.h"
#include "pxcsensemanager.h"
#include "pxccapture.h"
#include "pxcvideomodule.h"
#include "pxchandconfiguration.h"
#include "pxcprojection.h"
#include <iostream>
#include "GLM.h"
#include <vector>
#include "ShaderManager.h"
#include "Camera.h"
#include "Constants.h"

class RealSense
{
public:
	PXCSession *session;
	PXCSenseManager *pp;
	PXCHandConfiguration* config;
	PXCHandData* outputData;
	PXCProjection *projection;

	std::vector<PXCHandData::JointData> joints;
	std::vector<PXCPoint3DF32> uvz;

	std::vector<glm::vec3> handPoints;
	int numHandPoints;
	
	float maxRange;

	RealSense();
	~RealSense();

	void Update();
	void ConnectJoints();
	void DrawPoints(Camera &camera, ShaderManager &shaderManager);

};


#endif