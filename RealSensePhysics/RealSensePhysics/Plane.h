#ifndef PLANE_H
#define PLANE_H

#define GLM_FORCE_CUDA

#include "cuda.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include "GLM.h"
#include <vector>
#include "ParticleSystem.h"
#include "Constants.h"

class Plane
{
public:
	glm::vec3 point;
	glm::vec3 normal;

	Plane();
	Plane(glm::vec3 p, glm::vec3 n);

	void Constrain(int numParticles, ParticleSystem &particleSystem);
};


#endif