#ifndef SCENE_H
#define SCENE_H

#include "Camera.h"
#include "ShaderManager.h"
#include "CudaHeaders.h"
#include "PhysicsWorld.h"
#include "FluidRenderer.h"
#include "EnvironmentMap.h"
#include "RealSense.h"

class Scene
{
public:
	Camera camera;
	ShaderManager shaderManager;
	PhysicsWorld physicsWorld;
	FluidRenderer fluidRenderer;
	EnvironmentMap environmentMap;
	RealSense realSense;

	void Init(int screenWidth, int screenHeight);
	void Update();
	void Draw();
};


#endif