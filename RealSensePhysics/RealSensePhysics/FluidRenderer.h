#ifndef FLUIDRENDERER_H
#define FLUIDRENDERER_H

#include "PhysicsWorld.h"
#include "Camera.h"
#include "ShaderManager.h"
#include "EnvironmentMap.h"

enum RenderLayer {depthLayer, depthBlurLayer, normalsLayer, thickLayer, thickBlurLayer, reflectLayer, refractLayer, finalLayer, colourLayer, sphereLayer};

class FluidRenderer
{
public:
	int screenWidth;
	int screenHeight;
	GLuint sphereTex, depthTex, XdepthBlurTex, depthBlurTex, normalsBlurTex, thickTex, thickBlurTex, XthickBlurTex;
	GLuint frameBuffer;
	RenderLayer layer;

	void Init(int xRes, int yRes);
	GLuint NewFrameBuffer();
	void GenerateTextures();
	GLuint GenerateBufferTexture();
	void ChangeTexture(GLuint tex);
	void RenderFrameBuffer(GLuint fromTex, GLuint toTex, Camera &camera, GLuint shaderProgramID);
	void Render(PhysicsWorld &physicsWorld, EnvironmentMap &environmentMap, Camera &camera, ShaderManager &shaderManager);
	void RenderFinalBuffer(EnvironmentMap &environmentMap, Camera &camera, GLuint shaderProgramID);
	void DrawSpheres(PhysicsWorld &physicsWorld, Camera &camera, GLuint shaderProgramID);
};


#endif