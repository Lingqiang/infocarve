#include "Plane.h"

Plane::Plane() { }


Plane::Plane(glm::vec3 p, glm::vec3 n)
{
	point = p;
	normal = n;
}


__global__ void CudaPlaneConstrain(int numParticles, ParticleSystem particleSystem, float particleRadius, glm::vec3 planePoint, glm::vec3 planeNormal, float frictionCoefficient)
{
	int tid = threadIdx.x + (blockIdx.x * blockDim.x);

	if (tid < numParticles)
	{
		float dist = glm::dot((particleSystem.newPositions[tid] - planePoint), planeNormal);

		if (glm::abs(dist) < particleRadius && particleSystem.newPositions[tid].y < 0.0f)
		{
			particleSystem.newPositions[tid] -= (dist - particleRadius) * planeNormal;

			glm::vec3 relativeDisp = particleSystem.newPositions[tid] - particleSystem.positions[tid];
			glm::vec3 tangentialDisp = relativeDisp - (glm::dot(relativeDisp, planeNormal) * planeNormal);

			particleSystem.newPositions[tid] -= tangentialDisp * frictionCoefficient;
		}
	}
}

void Plane::Constrain(int numParticles, ParticleSystem &particleSystem)
{
	CudaPlaneConstrain<<< (numParticles + (threadsPerBlock - 1)) / threadsPerBlock, threadsPerBlock>>> (numParticles, particleSystem, particleRadius, point, normal, boundaryFriction);
}