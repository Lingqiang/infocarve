#ifndef FLUID_H
#define FLUID_H

#include "CudaHeaders.h"
#include "ParticleSystem.h"
#include <vector>
#include "Constants.h"

#define MAX_NEIGHBOURS 40
#define PARTICLE_DIAMETER 0.08f
#define PARTICLE_DIAMETER_2 0.0064f
#define PARTICLE_DIAMETER_6 0.000000262144f
#define PARTICLE_DIAMETER_9 0.000000000134217728f
#define REST_DENSITY 1000.0f
#define RELAXATION 0.0003f
#define MAX_FLUID_PARTICLES 100000

class Fluid
{
public:
	int numFluidParticles;
	float restDensity;
	int startingIndex;

	int* cudaFluidParticles;

	Fluid();
	Fluid(int num, ParticleSystem &particleSystem);
	Fluid(bool hose);

	void Hose(ParticleSystem &particleSystem);

	void Constrain(ParticleSystem &particleSystem, int* neighbours, int* numNeighbours);
	void ModifyVelocity(ParticleSystem &particleSystem, int* neighbours, int* numNeighbours, float timestep);
};




#endif