#include "Constants.h"


float timestep = 0.008f;
float gravity = -9.8f;
float boundsExtent = 5.0f;
float particleRadius = 0.04f;
float particleDiameter = particleRadius * 2.0f;
float boundaryFriction = 0.0f;

int threadsPerBlock = 256;