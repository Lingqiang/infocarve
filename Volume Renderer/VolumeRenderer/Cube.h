#ifndef CUBE_H
#define CUBE_H

// Very simple class for the sake of having similar classes in the Custom Renderer examples
// (also implemented for other platforms)
class Cube
{
	public:
		void render();
};

#endif