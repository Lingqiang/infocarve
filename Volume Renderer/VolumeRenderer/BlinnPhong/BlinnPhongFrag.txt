#version 300 es
precision mediump float;

uniform sampler3D volume;
uniform int maxRaySteps;
uniform float rayStepSize;
uniform float gradientStepSize;

uniform vec3 camPos;

// uniform float materialShininess; //0.5f
// uniform float materialKD; //0.5f
// uniform float materialKS; //0.69


uniform float uOpacity;//0.33 skull, 0.2 head


in vec3 facePos;
out vec4 FragColor;

vec3 CalculateNormal(vec3 texCoord)
{
	vec3 sample1, sample2;

	sample1.x = texture(volume, texCoord - vec3(gradientStepSize, 0.0f, 0.0f)).x;
	sample2.x = texture(volume, texCoord + vec3(gradientStepSize, 0.0f, 0.0f)).x;
	sample1.y = texture(volume, texCoord - vec3(0.0f, gradientStepSize, 0.0f)).x;
	sample2.y = texture(volume, texCoord + vec3(0.0f, gradientStepSize, 0.0f)).x;
	sample1.z = texture(volume, texCoord - vec3(0.0f, 0.0f, gradientStepSize)).x;
	sample2.z = texture(volume, texCoord + vec3(0.0f, 0.0f, gradientStepSize)).x;

	return normalize(sample2 - sample1);
}

void main()
{
	vec4 finalColor = vec4(0.0f, 0.0f, 0.0f, 0.0f);
	vec3 position = facePos;
	vec3 LightDir = normalize(vec3(1.0f,1.0f,1.0f));
	vec3 texCoord;
	vec3 normal;
	
	vec4 colorFinal;
	
	float materialShininess = 0.5f;
	float materialKD = 0.5f;
	float materialKS = 0.69f;
	

	vec3 direction = position - camPos;
	direction = normalize(direction);

	float light;
	
	for(int i=0; i<maxRaySteps; i++)
	{
		texCoord = (position + 1.0f) / 2.0f; 

		float index = texture(volume, texCoord).x;
			
		if(index > uOpacity)
		{
			normal = CalculateNormal(texCoord);
			vec3 viewDir = direction;
			
			float LdotN = max(0.0f, dot(normal,LightDir ));
			float diffuse = materialKD * LdotN;
			float specular = 0.0f;
			
			vec3 H = normalize(LightDir + viewDir);

			if(LdotN > 0.0)
			{
				specular = materialKS * pow(max(0.0f,dot(H,normal)), materialShininess);
			}

			float VdotN = dot(viewDir,normal);

			float specMask = (pow(dot(H, normal), materialShininess) > 0.4f) ? 1.0f : 0.0f;		

			light =  (  diffuse + (specular * specMask));


			finalColor = vec4(light,light,light,1.0f);

		
			}
		break;
		}
		
		position = position + (direction * rayStepSize);

		if (abs(position.x) > 1.0f || abs(position.y) > 1.0f || abs(position.z) > 1.0f)
			break;
	}

	FragColor = finalColor;
}