/*
 * BackgroundRenderer.h
 *
 *  Created on: 24 Feb 2015
 *      Author: GV2LabPC
 */

#ifndef BACKGROUNDRENDERER_H_
#define BACKGROUNDRENDERER_H_

#include <GL/glew.h>
#include <windows.h>

#include "opencv2/opencv.hpp"
#include "opencv2/imgproc/imgproc.hpp"

class BackgroundRenderer
{
public:

	void Init();
	void DrawBackground(int shaderID, GLuint backgroundTex, GLuint volumeTex, IplImage * inDepthImage);

	GLuint mVboTexCoords;
	GLuint mVboPosition;
	GLuint mVao;
	bool mBlackAndWhite;


private:

};


#endif /* BACKGROUNDRENDERER_H_ */
