#include "RealSense.h"

#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480
#define DEPTH_WIDTH 640
#define DEPTH_HEIGHT 480

using namespace cv;
/*
RealSense::RealSense()
{
session = PXCSession::CreateInstance();
if (session == NULL) 
{
std::cout << "Failed to create an SDK session of Hands Viewer" << std::endl;
return;
}

pp = session->CreateSenseManager();
if(!pp)
{
std::cout << "Failed to create SenseManager" << std::endl;
return;
}


bool sts=true;
// Set Module 
pxcStatus status = pp->EnableHand(0);
PXCHandModule *handAnalyzer=pp->QueryHand();
if(handAnalyzer == NULL || status != pxcStatus::PXC_STATUS_NO_ERROR)
{
std::cout << "Failed to pair the gesture module with I/O" << std::endl;
return;
}

if (pp->Init() >= PXC_STATUS_NO_ERROR) 
{
outputData = handAnalyzer->CreateOutput();

// IF IVCAM Set the following properties
PXCCapture::Device *device=pp->QueryCaptureManager()->QueryDevice();
PXCCapture::DeviceInfo dinfo;
pp->QueryCaptureManager()->QueryDevice()->QueryDeviceInfo(&dinfo);
if(dinfo.model == PXCCapture::DEVICE_MODEL_IVCAM)
{
device->SetDepthConfidenceThreshold(1);
device->SetMirrorMode(PXCCapture::Device::MIRROR_MODE_DISABLED);
device->SetIVCAMFilterOption(6);
}

maxRange = pp->QueryCaptureManager()->QueryDevice()->QueryDepthSensorRange().max;

// Hand Module Configuration
config = handAnalyzer->CreateActiveConfiguration();	
config->EnableNormalizedJoints(false);

//		if(showExtremityPoint) 
//			config->SetTrackingMode(PXCHandData::TRACKING_MODE_EXTREMITIES);

config->EnableAllAlerts();		
config->EnableSegmentationImage(true);	
config->ApplyChanges();
config->Update();

pxcI32 totalNumOfGestures = config->QueryGesturesTotalNumber();
if (totalNumOfGestures > 0)
{
//			SetCMBGesturePos(hwndDlg);
//			for (int i = 0; i < totalNumOfGestures; i++)
//			{
//				pxcCHAR* gestureName= new pxcCHAR[PXCHandData::MAX_NAME_SIZE];
//				if (config->QueryGestureNameByIndex(i,PXCHandData::MAX_NAME_SIZE, gestureName) ==	pxcStatus::PXC_STATUS_NO_ERROR)
//				{
//					AddCMBItem(hwndDlg,gestureName);
//				}
//				delete[] gestureName;
//				gestureName = NULL;
//			}
//			EnableCMBItem(hwndDlg,true);
}

std::cout << "Streaming" << std::endl;
}
else
{
std::cout << "RealSense Camera Init Failed" << std::endl;
}

joints.resize(PXCHandData::NUMBER_OF_JOINTS * 2);
handPoints.resize((PXCHandData::NUMBER_OF_JOINTS + 50) * 2);

}
*/


/*
void RealSense::Update()
{
numHandPoints = 0;

pxcStatus sts=pp->AcquireFrame(true);

if (sts < PXC_STATUS_NO_ERROR) 
return;


outputData->Update();

const PXCCapture::Sample *sample = pp->QueryHandSample();
if (sample && sample->depth)
{
//Iterate hands
for(int i = 0 ; i < outputData->QueryNumberOfHands() ; i++)
{
//Get hand by time of appearence
PXCHandData::IHand* handData;
if(outputData->QueryHandData(PXCHandData::AccessOrderType::ACCESS_ORDER_BY_TIME,i,handData) == PXC_STATUS_NO_ERROR)
{
PXCHandData::JointData jointData;
//Iterate Joints
for(int j = 0; j < PXCHandData::NUMBER_OF_JOINTS ; j++)
{
handData->QueryTrackedJoint((PXCHandData::JointType)j,jointData);	
//					std::cout << jointData.positionWorld.x << ", " << jointData.positionWorld.y << ", " << jointData.positionWorld.z << std::endl;
joints[(i*PXCHandData::NUMBER_OF_JOINTS) + j] = jointData;
}
}
}
}

pp->ReleaseFrame();

ConnectJoints();
}
*/

RealSense::RealSense()
{
	session = PXCSession::CreateInstance();
	if (session == NULL) 
	{
		std::cout << "Failed to create an SDK session of Hands Viewer" << std::endl;
		return;
	}

	pp = PXCSenseManager::CreateInstance();
	if(!pp)
	{
		std::cout << "Failed to create SenseManager" << std::endl;
		return;
	}

	
	
	pp->EnableStream(PXCCapture::STREAM_TYPE_DEPTH, SCREEN_WIDTH, SCREEN_HEIGHT, 30);
	pp->EnableStream(PXCCapture::STREAM_TYPE_COLOR, SCREEN_WIDTH, SCREEN_HEIGHT, 30);
	

	bool sts=true;
	// Set Module 


	if (pp->Init() >= PXC_STATUS_NO_ERROR) 
	{
		pp->QueryCaptureManager()->QueryDevice()->SetMirrorMode( PXCCapture::Device::MirrorMode::MIRROR_MODE_DISABLED );
		//		// IF IVCAM Set the following properties
		PXCCapture::Device *device=pp->QueryCaptureManager()->QueryDevice();
		PXCCapture::DeviceInfo dinfo;
		device->QueryDeviceInfo(&dinfo);

		projection = device->CreateProjection();
		
		

		std::cout << "Streaming" << std::endl;
	}
	else
	{
		std::cout << "RealSense Camera Init Failed" << std::endl;
	}

	handPoints.resize(SCREEN_WIDTH * SCREEN_HEIGHT);
	uvz.resize(SCREEN_WIDTH*SCREEN_HEIGHT);
}

RealSense::~RealSense()
{
	
	//if(projection)projection->Release();
	
	//if(session)session->Release();
	//if(pp)pp->Release();


	//	config->Release();
	//	outputData->Release();
	//	pp->Close();
	//	pp->Release();
}

glm::vec3 PXC3MMtoGLM(PXCPoint3DF32 pxcPoint)
{
	return glm::vec3(pxcPoint.x / 1000.0f, pxcPoint.y / 1000.0f, pxcPoint.z / 1000.0f);
}

bool RealSense::Update()
{
	numHandPoints = 0;

	pxcStatus sts=pp->AcquireFrame(true);

	if (sts < PXC_STATUS_NO_ERROR) 
		return FALSE;

	sample = pp->QuerySample();

	//PXCImage *image = (*sample)[PXCCapture::STREAM_TYPE_DEPTH];

	PXCImage::ImageData data;

	int numUVZ = 0;


	if(sample->depth && sample->color
		&& sample->depth->AcquireAccess( PXCImage::ACCESS_READ, PXCImage::PIXEL_FORMAT_DEPTH, &data) >= PXC_STATUS_NO_ERROR
		&& sample->color->AcquireAccess( PXCImage::ACCESS_READ, PXCImage::PIXEL_FORMAT_RGB24, &dataColor) >= PXC_STATUS_NO_ERROR)
	{

		short invalids[1];
		//	pp->QueryCaptureManager()->QueryDevice()->
		//	invalids[0] = pp->QueryCaptureManager()->QueryDevice()->QueryDepthSaturationValue();
		short invalid = pp->QueryCaptureManager()->QueryDevice()->QueryDepthLowConfidenceValue();

		PXCImage::ImageInfo info = sample->depth->QueryInfo();


		short *dpixels=(short*)data.planes[0];
		int dpitch = data.pitches[0] / sizeof(short);

		for (int y = 0; y < (int)info.height; y+=2)
		{
			for (int x = 0; x < (int)info.width; x+=2)
			{
				short d = dpixels[y*dpitch+x];

				if (d == invalid) 
					continue;

				PXCPoint3DF32 point;
				point.x = x;
				point.y = y;
				point.z = d;

				if (d > 0 && d <1000)
					uvz[numUVZ++] = point;
			}
		}


		std::vector<PXCPoint3DF32> worldCoords(numUVZ);
		depthCoord2color.resize(numUVZ);
		projection->ProjectDepthToCamera(numUVZ, &uvz[0], &worldCoords[0]);
		projection->MapDepthToColor(numUVZ,&uvz[0],&depthCoord2color[0]);

		for(int i=0; i<numUVZ; i++)
		{
			handPoints[numHandPoints++] = PXC3MMtoGLM(worldCoords[i]);
		}

		for (int i=0; i<numHandPoints; i++)
		{
			handPoints[i] *= 3.0f;
			float x = handPoints[i].x;
			float y = handPoints[i].y;
			float z = handPoints[i].z;
			handPoints[i] = glm::vec3(-x, y, -z);
		}


		sample->depth->ReleaseAccess(&data);


		return TRUE;


	}
	else{	
		return FALSE;}



}


glm::vec3 PXC3toGLM(PXCPoint3DF32 pxcPoint)
{
	return glm::vec3(pxcPoint.x, pxcPoint.y, pxcPoint.z);
}

void RealSense::ConnectJoints()
{
	for(int i = 0 ; i < outputData->QueryNumberOfHands() ; i++)
	{
		glm::vec3 wristPos = PXC3toGLM(joints[i * PXCHandData::NUMBER_OF_JOINTS].positionWorld);

		handPoints[numHandPoints++] = wristPos;

		for (int j=1; j<PXCHandData::NUMBER_OF_JOINTS; j++) 			
		{
			if(joints[(i*PXCHandData::NUMBER_OF_JOINTS) + j].confidence == 0) 
				continue;

			glm::vec3 pos = PXC3toGLM(joints[(i * PXCHandData::NUMBER_OF_JOINTS) + j].positionWorld);

			if(j == 2 || j == 6 || j == 10 || j == 14 || j == 18)
			{
				for (int k=1; k<=4; k++)
				{
					handPoints[numHandPoints++] = wristPos + ((pos - wristPos) * ((float)k / 4.0f));
				}
			}
			else if(j == 5 || j == 9 || j == 13 || j == 17 || j == 21)
			{
				handPoints[numHandPoints++] = pos;
			}
			else
			{
				glm::vec3 prevPos = PXC3toGLM(joints[(i * PXCHandData::NUMBER_OF_JOINTS) + j - 1].positionWorld);

				handPoints[numHandPoints++] = prevPos + ((pos - prevPos) / 2.0f);
				handPoints[numHandPoints++] = pos;
			}
		}
	}

	for (int i=0; i<numHandPoints; i++)
	{
		handPoints[i] *= 3.0f;
		float x = handPoints[i].x;
		float y = handPoints[i].y;
		float z = handPoints[i].z;
		handPoints[i] = glm::vec3(-x, -z, y);
	}
}

void RealSense::DrawPoints(Camera &camera, ShaderManager &shaderManager)
{
	glClearColor(0.0f,0.0f,0.0f,0.0f);
	GLuint shaderProgramID = shaderManager.UseShader(SimpleShader);

	int uniformLoc;

	glm::mat4 model_mat = glm::mat4(1.0f);

	uniformLoc = glGetUniformLocation (shaderProgramID, "proj");
	glUniformMatrix4fv (uniformLoc, 1, GL_FALSE, &camera.projMat[0][0]);

	uniformLoc = glGetUniformLocation (shaderProgramID, "view");
	glUniformMatrix4fv (uniformLoc, 1, GL_FALSE, &camera.viewMat[0][0]);

	uniformLoc = glGetUniformLocation (shaderProgramID, "model");
	glUniformMatrix4fv (uniformLoc, 1, GL_FALSE, &model_mat[0][0]);

	int texcoords_location = glGetAttribLocation (shaderProgramID, "vTexture");
	int radius_location = glGetAttribLocation (shaderProgramID, "pRadius");	

	//	glm::mat4 upMat = glm::rotate(glm::mat4(), -camera.pitch, glm::vec3(1, 0, 0));
	//	glm::vec3 upVec = glm::rotate(glm::quat_cast(upMat), glm::vec3(0, 1, 0));

	//glm::vec3 camDirection = camera.GetViewDirection();

	//glm::vec3 rightVec = glm::normalize(glm::cross(camDirection, glm::vec3(0.0f, 1.0f, 0.0f)));
	//glm::vec3 upVec = glm::normalize(glm::cross(camDirection, -rightVec));

	//	glBegin(GL_QUADS);
	//	for (int i=0; i<numHandPoints; i++)
	//	{
	//		glm::vec3 posToCam = glm::normalize(handPoints[i] - camera.position);
	//		glm::vec3 crossCam = glm::cross(posToCam, upVec);
	//
	//		float radius = particleRadius * 0.25f;
	//		glVertexAttrib1f(radius_location, radius);
	//
	//		glm::vec3 corner = handPoints[i] - (radius * crossCam);
	//		corner += radius*upVec;
	//
	//		glColor4f(0.0f, 1.0f, 1.0f, 1.0f);
	//		glVertexAttrib2f(texcoords_location, 0.0f, 0.0f);
	//		glVertex3f(corner.x, corner.y, corner.z);
	//
	//		corner -= 2.0f*radius*upVec;
	//
	//		glColor4f(0.0f, 1.0f, 1.0f, 1.0f);
	//		glVertexAttrib2f(texcoords_location, 0.0f, 1.0f);
	//		glVertex3f(corner.x, corner.y, corner.z);
	//
	//		corner = handPoints[i] + (radius * crossCam);
	//		corner -= radius*upVec;
	//
	//		glColor4f(0.0f, 1.0f, 1.0f, 1.0f);
	//		glVertexAttrib2f(texcoords_location, 1.0f, 1.0f);
	//		glVertex3f(corner.x, corner.y, corner.z);
	//
	//		corner += 2.0f*radius*upVec;
	//
	//		glColor4f(0.0f, 1.0f, 1.0f, 1.0f);
	//		glVertexAttrib2f(texcoords_location, 1.0f, 0.0f);
	//		glVertex3f(corner.x, corner.y, corner.z);
	//	}
	//	glEnd();


	//float r,g,b;
	//char *cpixels=(char*)dataColor.planes[0];

	GLint colorLoc = glGetAttribLocation(shaderProgramID, "vColor");
	IplImage * backgroundImage8u3 = cvCreateImageHeader(cvSize(SCREEN_WIDTH,SCREEN_HEIGHT),IPL_DEPTH_8U,3);
	cvSetData(backgroundImage8u3,dataColor.planes[0],dataColor.pitches[0]);

	//glPointSize(2.0f);
	glBegin(GL_POINTS);
	for (int i=0; i<numHandPoints; i++)
	{
		if(depthCoord2color[i].y > 0 && depthCoord2color[i].x > 0){
			//b = cpixels[(int)depthCoord2color[i].y*dataColor.pitches[0] + (int)depthCoord2color[i].x*3]/255.0f;
			//g = cpixels[(int)depthCoord2color[i].y*dataColor.pitches[0] + (int)depthCoord2color[i].x*3 + 1]/255.0f;
			//r = cpixels[(int)depthCoord2color[i].y*dataColor.pitches[0] + (int)depthCoord2color[i].x*3 + 2]/255.0f;
			//glColor4f(r,g,b,1.0f);
			//glColor3f(1.0f, 0.0f, 0.0f);
			CvScalar tcolor = cvGet2D(backgroundImage8u3,depthCoord2color[i].y,depthCoord2color[i].x);
			glVertexAttrib3f(colorLoc, tcolor.val[2]/255.0f, tcolor.val[1]/255.0f, tcolor.val[0]/255.0f);
			//glVertexAttrib3f(colorLoc, r, g, b);
			glVertex3f(-handPoints[i].x, handPoints[i].y, handPoints[i].z);
		}
	}
	glEnd();

	cvReleaseImageHeader(&backgroundImage8u3);
	sample->color->ReleaseAccess(&dataColor);
	pp->ReleaseFrame();
}
