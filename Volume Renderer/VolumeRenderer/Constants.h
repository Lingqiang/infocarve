#ifndef CONSTANTS_H
#define CONSTANTS_H

extern float timestep;
extern float gravity;
extern float boundsExtent;
extern float particleRadius;
extern float particleDiameter;
extern float boundaryFriction;

extern int threadsPerBlock;

#define MAX_PARTICLES 10000000
#define MAX_NEIGHBOURS 30


#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480
#define DEPTH_WIDTH 640
#define DEPTH_HEIGHT 480

#endif